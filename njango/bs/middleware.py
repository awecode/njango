from django.utils.deprecation import MiddlewareMixin

from .utils import SESSION_KEY, get_calendar


class CalendarMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if not SESSION_KEY in request.session or request.session[SESSION_KEY] is None:
            request.session[SESSION_KEY] = get_calendar(request)
