# -*- coding: utf-8 -*-

import re

from django.conf import settings
from django.utils import translation

SESSION_KEY = 'cal'


def get_default_calendar():
    try:
        return settings.DEFAULT_CALENDAR
    except AttributeError:
        return 'ad'


def get_calendar(request):
    for attr in ('session', 'COOKIES'):
        if hasattr(request, attr):
            try:
                return getattr(request, attr)[SESSION_KEY]
            except KeyError:
                continue
    return get_default_calendar()


def ne2en(num, reverse=False):
    if num is None:
        return None
    dct = {
        u'०': '0',
        u'१': '1',
        u'२': '2',
        u'३': '3',
        u'४': '4',
        u'५': '5',
        u'६': '6',
        u'७': '7',
        u'८': '8',
        u'९': '9'
    }
    if reverse:
        dct = {v: k for k, v in dct.items()}
    pattern = re.compile('|'.join(dct.keys()))
    grouper = lambda x: dct[x.group()]
    num = str(num)
    result = pattern.sub(grouper, num)
    return result


def en2ne(n):
    return ne2en(n, reverse=True)


def transl(st, lang=None):
    lang = lang or translation.get_language()
    lang = lang.split('-')[0]
    if lang == 'en':
        return ne2en(st)
    elif lang == 'ne':
        return en2ne(st)
