from django.utils.http import is_safe_url
from django import http
from django.views.generic import DetailView

from .nepdate import string_from_tuple, ad2bs, bs2ad
from .widgets import BSWidget

from .utils import get_calendar, SESSION_KEY, get_default_calendar


def change_calendar(request):
    nxt = request.POST.get('next', request.GET.get('next'))
    if not is_safe_url(url=nxt, host=request.get_host()):
        nxt = request.META.get('HTTP_REFERER')
        if not is_safe_url(url=nxt, host=request.get_host()):
            nxt = '/'
    response = http.HttpResponseRedirect(nxt)
    if request.method == 'POST':
        cal = request.POST.get('calendar')
        if cal and hasattr(request, 'session'):
            request.session[SESSION_KEY] = cal
    return response


class BSDateView:
    bs_fields = []
    cal = get_default_calendar()
    parsed = False

    def get_form(self, form_class=None):
        self.cal = get_calendar(self.request)
        form = super(BSDateView, self).get_form(form_class=form_class)

        bs_fields = self.model.BSMeta.fields if hasattr(self.model, 'BSMeta') and hasattr(self.model.BSMeta, 'fields') else []
        form_fields = list(form.fields.keys())
        # Get common fields (intersection) from model meta and form fields
        fields = list(set(bs_fields).intersection(form_fields))
        for field_name in fields:
            self.bs_fields.append(field_name)
            if self.cal == 'bs' and not self.parsed:
                value = form[field_name].value()
                if value:
                    if self.request.method == 'GET':
                        new_val = string_from_tuple(ad2bs(value))
                        form[field_name].initial = new_val
                        form.fields[field_name].initial = new_val
                        setattr(form.instance, field_name, new_val)
                    elif self.request.method == 'POST' and form.is_valid():
                        form.data = form.data.copy()
                        form.data[field_name] = string_from_tuple(bs2ad(value))
                        setattr(form.instance, field_name, form.data[field_name])
            form.fields[field_name].widget = BSWidget(cal=self.cal, attrs=form.fields[field_name].widget.attrs)
        return form

    def get_object(self, queryset=None):
        obj = super(BSDateView, self).get_object()
        if not self.request.method == 'POST':
            # Check if detail view
            # if isinstance(self, DetailView):
            self.cal = get_calendar(self.request)
            if self.cal == 'bs' and not self.parsed:
                bs_fields = self.model.BSMeta.fields if hasattr(self.model, 'BSMeta') and hasattr(self.model.BSMeta,
                                                                                                  'fields') else []
                for field_name in bs_fields:
                    setattr(obj, field_name, string_from_tuple(ad2bs(getattr(obj, field_name))))
                self.parsed = True

        return obj
