from django.forms import TextInput, Media
from django.utils.safestring import mark_safe


class BSWidget(TextInput):
    cal = 'ad'

    def __init__(self, attrs=None, cal='ad'):
        self.cal = cal
        super(BSWidget, self).__init__(attrs)

    # def render(self, name, value, attrs=None):
    #     html = super().render(name, value, attrs)
    #     el_id = self.build_attrs(attrs).get('id')
    #     html += self.trigger_picker(el_id)
    #     return mark_safe(html)
    # 
    # def trigger_picker(self, el_id):
    #     if self.cal == 'bs':
    #         str = """
    #         <script>
    #             $(function(){
    #                 $('#%s').nepaliDatePicker();
    #             });
    #         </script>""" % el_id
    #     elif self.cal == 'ad':
    #         str = """
    #         <script>
    #             $(function(){
    #                 $('#%s').datepicker({
    #                     format: 'yyyy-mm-dd',
    #                 });
    #             });
    #         </script>""" % el_id
    #     return str

    def _media(self):
        if self.cal == 'bs':
            css = {
                'all': ('njango/css/nepali.datepicker.css',)
            }
            js = ('njango/js/nepali.datepicker.js', 'njango/js/bs_datepicker_init.js')
        elif self.cal == 'ad':
            css = {
                'all': ('njango/css/ad_datepicker.css',)
            }
            js = ('njango/js/ad_datepicker.js', 'njango/js/ad_datepicker_init.js')

        return Media(css=css, js=js)

    media = property(_media)
