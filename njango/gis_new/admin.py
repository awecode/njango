from django.contrib import admin
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe

from .models import Province, District, VillageOrMunicipalCouncil


class ProvinceAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'name_ne',)
    search_fields = ('number', 'name', 'name_ne', 'alt_names')


class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name', '_province')
    search_fields = ('name', 'province__name', 'alt_names')
    list_filter = ('province',)

    def get_queryset(self, request):
        return super(DistrictAdmin, self).get_queryset(request).select_related('province')

    def _province(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_new_province_change', args=(obj.province_id,)), obj.province))

    _province.admin_order_field = 'province__name'


class VillageOrMunicipalCouncilAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', '_district', '_province')
    search_fields = ('name', 'district__name', 'district__province__name', 'type')
    list_filter = ('type', 'district', 'district__province')

    def get_queryset(self, request):
        return super(VillageOrMunicipalCouncilAdmin, self).get_queryset(request).select_related('district__province')

    def _district(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_new_district_change', args=(obj.district_id,)), obj.district))

    _district.admin_order_field = 'district__name'

    def _province(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (
            reverse_lazy('admin:gis_new_province_change', args=(obj.district.province_id,)), obj.district.province))

    _province.admin_order_field = 'district__province__name'


admin.site.register(Province, ProvinceAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(VillageOrMunicipalCouncil, VillageOrMunicipalCouncilAdmin)
