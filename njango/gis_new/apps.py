from django.apps import AppConfig


class GisNewConfig(AppConfig):
    name = 'njango.gis_new'
    verbose_name = 'Nepal GIS (New)'
