from django.contrib.gis.db.models import MultiPolygonField
from django.db import models

VILLAGE_OR_MUNICIPAL_TYPES = (
    ('Village', 'Village'),
    ('Municipal Council', 'Municipal Council')
)


class Province(models.Model):
    number = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=75, blank=True, null=True)
    name_ne = models.CharField(max_length=75, blank=True, null=True)
    alt_names = models.CharField(max_length=255, blank=True, null=True, verbose_name='Alternative names')
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name or self.name_ne or str(self.number)

    class Meta:
        db_table = 'njango_gis_new_province'


class District(models.Model):
    name = models.CharField(max_length=75)
    province = models.ForeignKey(Province, on_delete=models.CASCADE)
    alt_names = models.CharField(max_length=255, blank=True, null=True, verbose_name='Alternative names')
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_new_district'


class VillageOrMunicipalCouncil(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(choices=VILLAGE_OR_MUNICIPAL_TYPES, max_length=12, default='Village')
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_new_adm4'
        # verbose_name = 'VDC or Municipality'
        # verbose_name_plural = 'VDCs or Municipalities'
