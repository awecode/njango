from django.contrib import admin
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe

from .models import Region, Zone, District, VDCOrMunicipality


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_ne',)
    search_fields = ('name', 'name_ne', 'alt_names')


class ZoneAdmin(admin.ModelAdmin):
    list_display = ('name', '_region')
    search_fields = ('name', 'region__name', 'alt_names')
    list_filter = ('region',)

    def get_queryset(self, request):
        return super(ZoneAdmin, self).get_queryset(request).select_related('region')

    def _region(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_old_region_change', args=(obj.region_id,)), obj.region))

    _region.admin_order_field = 'region__name'


class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name', '_zone', '_region')
    search_fields = ('name', 'zone__name', 'zone__region__name', 'alt_names')
    list_filter = ('zone', 'zone__region',)

    def get_queryset(self, request):
        return super(DistrictAdmin, self).get_queryset(request).select_related('zone__region')

    def _zone(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_old_zone_change', args=(obj.zone_id,)), obj.zone))

    _zone.admin_order_field = 'zone__name'

    def _region(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_old_region_change', args=(obj.zone.region_id,)), obj.zone.region))

    _region.admin_order_field = 'zone__region__name'


class VDCOrMunicipalityAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', '_district', '_zone', '_region')
    search_fields = ('name', 'district__name', 'district__zone__name', 'district__zone__region__name', 'type')
    list_filter = ('type', 'district', 'district__zone', 'district__zone__region')

    def get_queryset(self, request):
        return super(VDCOrMunicipalityAdmin, self).get_queryset(request).select_related('district__zone__region')

    def _district(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_old_district_change', args=(obj.district_id,)), obj.district))

    _district.admin_order_field = 'district__name'

    def _zone(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (reverse_lazy('admin:gis_old_zone_change', args=(obj.district.zone_id,)), obj.district.zone))

    _zone.admin_order_field = 'zone__name'

    def _region(self, obj):
        return mark_safe(
            '<a href="%s">%s</a>' % (
                reverse_lazy('admin:gis_old_region_change', args=(obj.district.zone.region_id,)), obj.district.zone.region))

    _region.admin_order_field = 'zone__region__name'


admin.site.register(Region, RegionAdmin)
admin.site.register(Zone, ZoneAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(VDCOrMunicipality, VDCOrMunicipalityAdmin)
