from django.apps import AppConfig


class GisOldConfig(AppConfig):
    name = 'njango.gis_old'
    verbose_name = 'Nepal GIS (Old)'
