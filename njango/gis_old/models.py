from django.contrib.gis.db.models import MultiPolygonField
from django.db import models

VDC_OR_MUNICIPALITY_TYPES = (
    ('VDC', 'VDC'),
    ('Municipality', 'Municipality')
)


class Region(models.Model):
    name = models.CharField(max_length=75)
    name_ne = models.CharField(max_length=200, null=True, verbose_name='Name in Nepali')
    alt_names = models.CharField(max_length=255, blank=True, null=True, verbose_name='Alternative names')
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_old_region'


class Zone(models.Model):
    name = models.CharField(max_length=75)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    alt_names = models.CharField(max_length=255, blank=True, null=True, verbose_name='Alternative names')
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_old_zone'


class District(models.Model):
    name = models.CharField(max_length=75)
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    alt_names = models.CharField(max_length=255, blank=True, null=True, verbose_name='Alternative names')
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_old_district'


class VDCOrMunicipality(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(choices=VDC_OR_MUNICIPALITY_TYPES, max_length=12, default='VDC')
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    geom = MultiPolygonField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'njango_gis_old_adm4'
        verbose_name = 'VDC or Municipality'
        verbose_name_plural = 'VDCs or Municipalities'
