from setuptools import setup

setup(
    name='njango',
    version='0.2',
    author='Dipesh Acharya',
    author_email='xtranophilist@gmail.com',
    packages=['njango'],
    url='https://github.com/awecode/njango/',
    license='BSD License',
    description='njango - the j is silent. Django app to make creating Nepali apps a breeze.',
    long_description=open('README.rst').read(),
    include_package_data=True,
    zip_safe=False,
    keywords='njango, django, nepali, bikram samvat, nepal',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6.1',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)